package com.capgemini.bookshop.transfer.mapper;

import com.capgemini.bookshop.persistence.entity.BookEntity;
import com.capgemini.bookshop.transfer.to.BookTo;

import java.util.List;

public interface BookMapper {

    BookTo map2To(BookEntity bookEntity);

    BookEntity map(BookTo bookTo);

    List<BookTo> map2To(List<BookEntity> bookEntities);

    List<BookEntity> map(List<BookTo> bookTos);
}
