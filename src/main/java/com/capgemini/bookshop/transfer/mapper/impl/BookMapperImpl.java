package com.capgemini.bookshop.transfer.mapper.impl;

import com.capgemini.bookshop.persistence.entity.BookEntity;
import com.capgemini.bookshop.transfer.mapper.BookMapper;
import com.capgemini.bookshop.transfer.to.BookTo;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class BookMapperImpl implements BookMapper {

    @Override
    public BookTo map2To(BookEntity bookEntity) {
        if (bookEntity != null) {
            return new BookTo(
                    bookEntity.getId(),
                    bookEntity.getTitle(),
                    bookEntity.getAuthor(),
                    bookEntity.getPublicationDate()
            );
        }
        return null;
    }

    @Override
    public BookEntity map(BookTo bookTo) {
        if (bookTo != null) {
            return new BookEntity(
                    bookTo.getTitle(),
                    bookTo.getAuthor(),
                    bookTo.getPublicationDate()
            );
        }
        return null;
    }

    @Override
    public List<BookTo> map2To(List<BookEntity> bookEntities) {
        return bookEntities.stream().map(this::map2To).collect(Collectors.toList());
    }

    @Override
    public List<BookEntity> map(List<BookTo> bookTos) {
        return bookTos.stream().map(this::map).collect(Collectors.toList());
    }
}
