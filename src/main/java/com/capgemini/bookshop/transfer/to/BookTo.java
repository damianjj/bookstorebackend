package com.capgemini.bookshop.transfer.to;

import lombok.*;

@Getter
@Setter
public class BookTo extends AbstractTo {

    @NonNull
    private String title;

    @NonNull
    private String author;

    @NonNull
    private String publicationDate;

    @Builder
    public BookTo(Long id, String title, String author, String publicationDate) {
        super(id);
        this.title = title;
        this.author = author;
        this.publicationDate = publicationDate;
    }
}
