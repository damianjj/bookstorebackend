package com.capgemini.bookshop.service;

import com.capgemini.bookshop.transfer.to.BookTo;

import java.util.List;

public interface BookService {

    List<BookTo> findAllBooks();

    List<BookTo> findPaginated(int page, int size);

    BookTo findBookById(Long id);

    BookTo saveBook(BookTo bookTo);

    BookTo updateBook(BookTo bookTo);

    void deleteBook(Long id);
}
