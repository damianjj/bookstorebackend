package com.capgemini.bookshop.service.impl;

import com.capgemini.bookshop.persistence.entity.BookEntity;
import com.capgemini.bookshop.persistence.repository.BookRepository;
import com.capgemini.bookshop.service.BookService;
import com.capgemini.bookshop.transfer.mapper.BookMapper;
import com.capgemini.bookshop.transfer.to.BookTo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class BookServiceImpl implements BookService {

    private BookRepository bookRepository;

    private BookMapper bookMapper;

    @Autowired
    public BookServiceImpl(BookRepository bookRepository, BookMapper bookMapper) {
        this.bookRepository = bookRepository;
        this.bookMapper = bookMapper;
    }

    @Override
    public List<BookTo> findAllBooks() {
        return bookMapper.map2To(bookRepository.findAll());
    }

    @Override
    public List<BookTo> findPaginated(int page, int size) {
        // TODO validation
        return bookMapper.map2To(bookRepository.findAll(PageRequest.of(page, size)).toList());
    }

    @Override
    public BookTo findBookById(Long id) {
        // TODO validation
        return bookMapper.map2To(bookRepository.getOne(id));
    }

    @Override
    @Transactional
    public BookTo saveBook(BookTo bookTo) {
        // TODO validation
        return bookMapper.map2To(bookRepository.save(bookMapper.map(bookTo)));
    }

    @Override
    @Transactional
    public BookTo updateBook(BookTo bookTo) {
        // TODO validation
        BookEntity bookEntity = bookRepository.findById(bookTo.getId()).orElse(null);

        if (bookEntity != null) {
            bookEntity.setTitle(bookTo.getTitle());
            bookEntity.setAuthor(bookTo.getAuthor());
            bookEntity.setPublicationDate(bookTo.getPublicationDate());
        }

        return bookMapper.map2To(bookEntity);
    }

    @Override
    @Transactional
    public void deleteBook(Long id) {
        // TODO validation
        bookRepository.deleteById(id);
    }
}
