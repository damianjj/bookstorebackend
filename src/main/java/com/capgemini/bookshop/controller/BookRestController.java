package com.capgemini.bookshop.controller;

import com.capgemini.bookshop.service.BookService;
import com.capgemini.bookshop.transfer.to.BookTo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/books")
public class BookRestController {

    private BookService bookService;

    @Autowired
    public BookRestController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping("")
    public ResponseEntity<List<BookTo>> getPaginated(@RequestParam int page, @RequestParam int size) {
        return ResponseEntity.ok().body(bookService.findPaginated(page, size));
    }

    @GetMapping("/{id}")
    public ResponseEntity<BookTo> getBookById(@PathVariable("id") Long id) {
        if (id <= 0) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(bookService.findBookById(id));
    }

    @PutMapping("")
    public ResponseEntity<BookTo> updateBook(@RequestBody BookTo bookTo) {
        return ResponseEntity.ok().body(bookService.updateBook(bookTo));
    }

    @PostMapping("")
    public ResponseEntity<BookTo> addBook(@RequestBody BookTo bookTo) {
        return ResponseEntity.created(URI.create("/" + bookService.saveBook(bookTo).getId())).body(null);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<BookTo> deleteBook(@PathVariable("id") Long id) {
        if (id <= 0) {
            return ResponseEntity.notFound().build();
        }
        bookService.deleteBook(id);
        return ResponseEntity.ok().build();
    }

}
