package com.capgemini.bookshop.persistence.repository;

import com.capgemini.bookshop.persistence.entity.BookEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookRepository extends JpaRepository<BookEntity, Long> {
    // TODO add update method
}
